import base64
import os
import requests
from flask import Flask
from openai import OpenAI
from flask import jsonify
from flask import send_from_directory
from flask import render_template 
import datetime

app = Flask(__name__)


def getPoem():
    api_key = os.getenv("OPENAI_API_KEY", None)

    if api_key is None:
        raise Exception("Missing Open API key.")
    
    client = OpenAI(
        api_key=api_key,
    )
    print("making request")
    completion = client.chat.completions.create(
    model="gpt-3.5-turbo",
    messages=[
        {"role": "system", "content": "You are a helpful assistant who writes valentines day poems in the form roses are red, violates are blue. Keep it short, no more than 6 lines, keep it playful. "},
        {"role": "user", "content": "Write me a valentine's day poem for someone named Nemo. She is a teacher and likes horses"}
    ]
    )

    print(completion.choices[0].message)
    return completion.choices[0].message

def getImage(filename):
    engine_id = "stable-diffusion-xl-1024-v1-0"
    api_host = os.getenv('API_HOST', 'https://api.stability.ai')
    api_key = os.getenv("STABILITY_API_KEY", None)


    if api_key is None:
        raise Exception("Missing Stability API key.")

    response = requests.post(
        f"{api_host}/v1/generation/{engine_id}/text-to-image",
        headers={
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": f"Bearer {api_key}"
        },
        json={
            "text_prompts": [
                {
                    "text": "A bouquet of flowers and horses that are comics"
                }
            ],
            "cfg_scale": 7,
            "height": 1024,
            "width": 1024,
            "samples": 1,
            "steps": 30,
        },
    )

    if response.status_code != 200:
        raise Exception("Non-200 response: " + str(response.text))

    data = response.json()

    for i, image in enumerate(data["artifacts"]):
        with open(f"./out/{filename}.png", "wb") as f:
            f.write(base64.b64decode(image["base64"]))

@app.route('/images/<path:path>')
def send_report(path):
    return send_from_directory('out', path)

@app.route('/assets/<path:path>')
def send_assets(path):
    return send_from_directory('web/assets', path)

@app.route('/')
def hello():
    return render_template('index.html',  
                           message="") 

@app.route('/api/generate')
def generate():
    print("Generating Image")
    basename = "image"
    suffix = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
    filename = "_".join([basename, suffix]) # e.g. 'mylogfile_120508_171442'
    getImage(filename)
    print("Image Generated, Generating Poem")
    poem = getPoem()
    print("Poem generated, passing payload")
    return jsonify({
        "poem": poem.content,
        "image": f"/images/{filename}.png"
    })