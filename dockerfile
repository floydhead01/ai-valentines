FROM python:3.12-alpine
EXPOSE 5000
WORKDIR /home/
COPY . . 
RUN pip install -r requirements.txt
ENTRYPOINT FLASK_APP=/home/app.py flask run --host=0.0.0.0