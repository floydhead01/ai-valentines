async function makePoem() {
    document.getElementById("card").style.display = "none";
    document.getElementById("intro").style.display = "none";
    document.getElementById("loading").style.display = "block";
    const response = await fetch("api/generate");
    const poem = await response.json();
    console.log(poem);
    document.getElementById("poem").innerHTML = poem.poem.replace(/\n/g, "<br>");
    document.getElementById("image").innerHTML = "<img src='" + poem.image + "'></img>"
    document.getElementById("loading").style.display = "none";
    document.getElementById("card").style.display = "block";
}

document.getElementById("button").addEventListener("click", ()=>{
    makePoem()
})
document.getElementById("button_regen").addEventListener("click", ()=>{
    makePoem()
})